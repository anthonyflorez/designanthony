

import 'package:flutter/material.dart';

import 'example1_page.dart';

class Example2Page extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Example2PageState();
  }
}

class _Example2PageState extends State<Example2Page> {
  final estilo = new TextStyle(fontSize: 25.0, color: Colors.blueAccent);
  final estilo2 = new TextStyle(fontSize: 25.0, color: Colors.greenAccent);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        contenedor1(),
        contenedor2(),
        contenedor3(),
        contenedor4(),
        contenedor5(),
      ],
    ));
  }

  Widget contenedor1() {
    return Container(
      color: HexColor('F0F5FC'),
    );
  }

  Widget contenedor2() {
    return Column(
      children: [
        Container(
            padding: EdgeInsets.only(top: 40.0, left: 20.0, right: 20.0),
            child: Column(
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(Icons.arrow_back_ios),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 50,
                            height: 50,
                            padding: EdgeInsets.only(right: 5.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: new Border.all(
                                              color: Colors.greenAccent,
                                              width: 3.0,
                                            ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                            child: Column(
                              children: [
                                Baseline(
                                  baseline: 8,
                                  baselineType: TextBaseline.alphabetic,
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            child: Center(child: Text('1',style:TextStyle(color:Colors.black,fontSize: 10))),
                                            width: 15,
                                            height: 15,
                                            
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: new Border.all(
                                                  color: Colors.greenAccent,
                                                  width: 3.0,
                                                ),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(100.0)))
                                                    ),
                                      ]),
                                ),
                                
                                Center(child: Icon(Icons.shopping_cart, color: Colors.grey))
                              ],
                            ),
                          ),
                          Container(
                            width: 50,
                            child: Center(child: Icon(Icons.menu, color: Colors.grey)),
                            height: 50,
                            padding: EdgeInsets.only(right: 5.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                          ),
                        ],
                      )
                    ]),
              ],
            )),
        Container(
          padding: EdgeInsets.only(left:30,top:10),
          child: Row(children: [
            CircleAvatar(
                radius: 30.0,
                child: Text('a'),
                backgroundImage: NetworkImage(
                    'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png')),
            Container( padding: EdgeInsets.only(left:10), child: Text('Robert Williamson',style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)))
          ]),
        )
      ],
    );
  }

  Widget contenedor3() {
    final size = MediaQuery.of(context).size;
    double ancho = size.width;
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
            padding: EdgeInsets.only(top: 50.0),
            decoration: BoxDecoration(
                color: HexColor('5E49DA'),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30.0),
                    topLeft: Radius.circular(30.0))),
            height: size.height * 0.75,
            width: ancho,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      padding: EdgeInsets.only(right: 5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                        Text('10%', style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold, fontSize: 30)),
                        Text('Discount',style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold, fontSize: 16))
                      ]),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.2),
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0))),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      padding: EdgeInsets.only(right: 0.0),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.2),
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Baseline(
                            baseline: -15,
                            baselineType: TextBaseline.alphabetic,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                      width: 25,
                                      child: Center(child: Text('1',style:TextStyle(color:Colors.white))),
                                      height: 25,
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)))),
                                ],

                                ),
                          ),
                          Container(child:Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                      Text('32\$', style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold, fontSize: 30)),
                      Text('Bonuses',style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold, fontSize: 16))
                      ]))
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                      Text('70\$', style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold, fontSize: 30)),
                      Text('Discount',style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold, fontSize: 16))
                      ]),
                      padding: EdgeInsets.only(right: 5.0),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.2),
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0))),
                    ),
                  ],
                ),
              ],
            )),
      ],
    );
  }

  Widget contenedor4() {
    final size = MediaQuery.of(context).size;
    double ancho = size.width;

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          padding: EdgeInsets.all(35.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(50.0),
                  topLeft: Radius.circular(50.0))),
          height: size.height * 0.5,
          width: ancho,
          child: GridView.count(
            // Create a grid with 2 columns. If you change the scrollDirection to
            // horizontal, this produces 2 rows.
            crossAxisCount: 2,
            // Generate 100 widgets that display their index in the List.
            children: [
              Card(
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.file_copy, size: 50.0),
                          Text('Orders History', style: TextStyle(fontSize: 16)),
                          
                        ]),
                  ),
                ),
              ),
              Card(
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.payment_outlined, size: 50.0),
                          Text('Payment Method', style: TextStyle(fontSize: 16))
                        ]),
                  ),
                ),
              ),
              Card(
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                           Baseline(
                              baseline: -40,
                              baselineType: TextBaseline.alphabetic,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                        child: Center(child: Text('1',style:TextStyle(color:Colors.white))),
                                        width: 20,
                                        height: 20,
                                        decoration: BoxDecoration(
                                            color: Colors.red,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100.0)))),
                                  ]),
                            ),
                          Icon(Icons.language, size: 50.0),
                          Text('Traking', style: TextStyle(fontSize: 16)),
                         
                        ]),
                  ),
                ),
              ),
              Card(
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                           Icon(Icons.pie_chart, size: 50.0),
                          Text('Stadistict', style: TextStyle(fontSize: 16))
                        ]),
                  ),
                ),
              ),
              Card(
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.build, size: 50.0),
                          Text('Stadistict', style: TextStyle(fontSize: 16))
                        ]),
                  ),
                ),
              ),
              Card(
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.health_and_safety , size: 50.0),
                          Text('Safety', style: TextStyle(fontSize: 16))
                        ]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget contenedor5() {
    return Column(children: [
      Row(children: [Text('hola')])
    ]);
  }
}
