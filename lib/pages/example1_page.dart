import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

final List<Widget> imageSliders = imgList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(item, fit: BoxFit.cover, width: 1000.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          'No. ${imgList.indexOf(item)} image',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();

final List<String> imgList = [
  'https://km.visamiddleeast.com/dam/VCOM/regional/ap/taiwan/global-elements/images/tw-visa-gold-card-498x280.png',
  'https://km.visamiddleeast.com/dam/VCOM/regional/ap/taiwan/global-elements/images/tw-visa-gold-card-498x280.png',
];

class Example1Page extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Example1PageState();
  }
}

class _Example1PageState extends State<Example1Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
      children: [
        contenedor1(),
        contenedor2(),
        contenedor3(),
      ],
    )
    );
  }


  Widget contenedor1() {
    return Container(
        padding: EdgeInsets.only(top:40.0, left: 20.0,right: 20.0),
        child: Column(
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Icon(Icons.arrow_back),
              Text('My Cards'),
              ElevatedButton(
                onPressed: () {},
                child: Icon(Icons.add),
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                  primary: HexColor("353FDE"),
                  padding: EdgeInsets.all(10),
                ),
              )
            ]),
          ],
        ));
  }

  Widget contenedor2() {
    return  Container(child: CarouselWithIndicatorDemo());
  }

  Widget contenedor3() {
    return Expanded(
        child: DefaultTabController(
        initialIndex: 0,
        length: 2,
        child:
         Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            bottom:const  PreferredSize(
              preferredSize: const Size.fromHeight(kToolbarHeight),
              child: Align(
                child:  TabBar(
                  labelPadding: EdgeInsets.symmetric(horizontal: 7.0),
                  indicatorColor: Colors.amber,
                  unselectedLabelColor: Colors.grey,
                  labelColor: Colors.amber,
                  tabs: <Widget>[
                    Tab(
                      text: 'Card Settings',
                    ),
                    Tab(
                      text: 'History',
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              Card(
                child: ListView(children: [
                  ListTile(
                      title: Text('ATM Withdraw'),
                      leading: Container(
                        width: 40,
                        height: 40,
                        color:  HexColor("#DCDDF6"),
                        child: Icon(Icons.print,color: HexColor("353FDE")),
                      ),
                      trailing: toogle(),
                      ),
                  ListTile(
                      title: Text('Online Payments'),
                      leading: Container(
                        width: 40,
                        height: 40,
                        color:  HexColor("F3F3FF"),
                        child: Icon(Icons.phone,color: HexColor("353FDE")),
                      ),
                      trailing: toogle(),
                      ),
                  ListTile(
                      title: Text('Block Card'),
                      leading: Container(
                        width: 40,
                        height: 40,
                        color:  HexColor("F3F3FF"),
                        child: Icon(Icons.block_flipped,color: HexColor("353FDE")),
                      ),
                      trailing: toogle(),
                      ),
                  ListTile(
                      title: Text('Reset Pin code'),
                      leading: Container(
                        width: 40,
                        height: 40,
                        color:  HexColor("F3F3FF"),
                        child: Icon(Icons.reset_tv,color: HexColor("353FDE")),
                      ),
                      trailing: toogle(),
                      ),
                ]),
              ),
              Center(
                child: Text("It's rainy here"),
              ),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            showSelectedLabels: false,
            iconSize: 32.0,
            showUnselectedLabels: false,
            unselectedItemColor: HexColor("DCDDF6"),
            selectedItemColor: HexColor("353FDE"),
            currentIndex: 1, // this will be set when a new tab is tapped
            items: [
              BottomNavigationBarItem(
                icon:  Icon(Icons.home),
                title:  Text('test'),
              ),
              BottomNavigationBarItem(
                icon:  Icon(Icons.mail),
                title:  Text('Messages'),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                   title: Text('Profile')
                   ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.menu),
                   title: Text('Profile')
                   )
            ],
          ),
        ),
      ));
      
  }

  toogle() {
    final isToggled =  false;
    return FlutterSwitch(
                      height: 20.0,
                      width: 40.0,
                      padding: 4.0,
                      activeColor: HexColor("353FDE"),
                      toggleSize: 15.0,
                      borderRadius: 10.0,
                      // activeColor: lets_cyan,
                      value: isToggled,
                      onToggle: (isToggled) {
                    setState(() {
                          isToggled = isToggled;
                    });
      });
  }
  int hexToInt(String hex)
{
  int val = 0;
  int len = hex.length;
  for (int i = 0; i < len; i++) {
    int hexDigit = hex.codeUnitAt(i);
    if (hexDigit >= 48 && hexDigit <= 57) {
      val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
    } else if (hexDigit >= 65 && hexDigit <= 70) {
      // A..F
      val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
    } else if (hexDigit >= 97 && hexDigit <= 102) {
      // a..f
      val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
    } else {
      throw new FormatException("Invalid hexadecimal value");
    }
  }
  return val;
}
}


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}



class CarouselWithIndicatorDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
   
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CarouselSlider(
            items: imageSliders,
            options: CarouselOptions(
              height: 200,
              autoPlay: true,
              enlargeCenterPage: true,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imgList.map((url) {
              int index = imgList.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                    ? Color.fromRGBO(0, 0, 0, 0.9)
                    : Color.fromRGBO(0, 0, 0, 0.4),
                ),
              );
            }).toList(),
          ),
        ]
      );
  }
}